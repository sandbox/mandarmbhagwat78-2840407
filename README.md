# SSL certificate validator

## Contents

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

## Introduction

This module allows user to verify SSL certificate .cert and .key files. It
returns certificate details if the certificate is valid.

## Requirements

This module does not have any dependencies.

## Installation

Like any other Drupal module install this module from the Drupal administration
modules page. For further information:
https://drupal.org/documentation/install/modules-themes/modules-8

## Configuration

Once installed and enabled you can go to /ssl-validator.
Enter valid SSL certificate key and certificate text. Click on "Verify" button.

## Maintainers

Current maintainers:
 * Mandar Bhagwat (mandarmbhagwat78) - https://www.drupal.org/u/mandarmbhagwat78
