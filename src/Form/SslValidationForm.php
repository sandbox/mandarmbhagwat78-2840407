<?php

namespace Drupal\ssl_validator\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements common submit handler used in SSL validation forms.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class SslValidationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ssl_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['ssl_configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('SSL certificate Configuration'),
      '#open' => TRUE,
    ];
    $form['ssl_configuration']['ssl_certificate_cert'] = [
      '#type' => 'textarea',
      '#title' => $this->t('SSL certificate text'),
      '#description' => $this->t("Enter the SSL certificate text."),
      '#required' => TRUE,
    ];
    $form['ssl_configuration']['ssl_certificate_private_key'] = [
      '#type' => 'textarea',
      '#title' => $this->t('SSL certificate private key'),
      '#description' => $this->t("Enter the SSL certificate private key text."),
      '#required' => TRUE,
    ];

    $form['ssl_configuration']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Validate'),
      '#ajax' => [
        'callback' => '::submitCallback',
        'wrapper' => 'ssl-msg-wrapper',
      ],
    ];

    $form['ssl_configuration']['ssl_msg'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'ssl-msg-wrapper'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Implements ajax submit callback.
   *
   * @param array $form
   *   Form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of the form.
   */
  public function submitCallback(array &$form, FormStateInterface $form_state) {
    $output = $this->ssCertificateVerify($form_state->getValue('ssl_certificate_cert'), $form_state->getValue('ssl_certificate_private_key'));

    $status = array_keys($output);
    $message = array_values($output);

    $form['ssl_configuration']['ssl_msg'][$status[0]] = [
      '#markup' => nl2br($message[0]),
    ];

    return $form['ssl_configuration']['ssl_msg'];
  }

  /**
   * Function to verify SSL certificate.
   */
  public function ssCertificateVerify($crt_text, $key_text) {
    if (empty($crt_text) || empty($key_text)) {
      return;
    }

    $crt_uri = file_default_scheme() . "://ssl.crt";
    $key_uri = file_default_scheme() . "://ssl.key";

    file_save_data($crt_text, $crt_uri);
    $crt_file = \Drupal::service('file_system')->realpath($crt_uri);

    file_save_data($key_text, $key_uri);
    $key_file = \Drupal::service('file_system')->realpath($key_uri);

    $result = array();
    if (!empty($crt_file) && !empty($key_file)) {
      $crt = shell_exec("openssl x509 -noout -modulus -in $crt_file | openssl md5");
      $key = shell_exec("openssl rsa -noout -modulus -in $key_file | openssl md5");

      if (empty($crt) && empty($key)) {
        $result['error'] = $this->t('OpenSSL is not installed.');;
      }
      else {
        if ($crt == $key) {
          $result['success'] = shell_exec("openssl x509 -in $crt_file -text -noout");
        }
      }
    }

    $this->sslCertificateDeleteFiles($crt_uri);
    $this->sslCertificateDeleteFiles($key_uri);

    if (empty($result)) {
      $result = array();
      $result['error'] = $this->t('Invalid certificate or key. Certificate and Key missmatched!');
    }

    return $result;
  }

  /**
   * Function to delete temporarily created files.
   */
  public function sslCertificateDeleteFiles($file_uri) {
    $query = db_select('file_managed', 'f')
      ->fields('f', array('fid'))
      ->condition('f.uri', $file_uri)
      ->execute();
    $fid = $query->fetchField();

    if ($fid) {
      $file_storage = \Drupal::entityManager()->getStorage('file');
      $file = $file_storage->load($fid);
      $file->delete();
    }
    else {
      file_unmanaged_delete($file_uri);
    }
  }

}
